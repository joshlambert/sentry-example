import React, { Component } from 'react';
import * as Sentry from '@sentry/browser';

function getRandomInt(max) {
  return Math.floor(Math.random() * Math.floor(max));
}

export default class Hello extends Component {
  state = {
    text: '',
  };

  componentDidMount() {
    Sentry.captureException(`error-${getRandomInt(10)}`);
  }

  render() {
    const { text } = this.state;
    return (
      <div>
        <button
          onClick={this.handleClick}
        >
          Hello
        </button>
        <div>{text}</div>
      </div>
    )
  }

  handleClick = () => {
    this.setState({
      text: 'Hello World',
    });
    try {
      throw new Error('Caught');
    } catch (err) {
      if (process.env.NODE_ENV !== 'production') {
        return;
      }
      Sentry.captureException(err);
    }
  }
}