import React, { Component } from 'react';
import './App.css';
import ErrorBoundary from './ErrorBoundary';
import Hello from './Hello';
import MyError from './MyError';
import MyRenderError from './MyRenderError';

class App extends Component {
  render() {
    return (
      <ErrorBoundary>
        <div className="App">
          <p className="App-intro">
            To get started, edit <code>src/App.js</code> and save to reload.
          </p>
          <Hello />
          <MyError />
          <MyRenderError />
        </div>
      </ErrorBoundary>
    );
  }
}

export default App;
